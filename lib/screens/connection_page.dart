import 'package:flutter/material.dart';

import 'activity_recognition_page.dart';

class ConnectionPage extends StatefulWidget {
  const ConnectionPage({Key? key}) : super(key: key);

  @override
  State<ConnectionPage> createState() => _ConnectionPageState();
}

class _ConnectionPageState extends State<ConnectionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Connection'),
      ),
      body: Center(
          child: ElevatedButton(
              onPressed: () {
                Navigator.of(context)
                    .pushNamed(ActivityRecognitionPage.routeName);
              },
              child: const Text('connect'))),
    );
  }
}
