import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:activity_recognition_app/widgets/activity_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class ActivityRecognitionPage extends StatefulWidget {
  final BluetoothDevice server;
  const ActivityRecognitionPage({Key? key, required this.server})
      : super(key: key);
  static const routeName = '/ActivityRecognitionScreen';
  @override
  State<ActivityRecognitionPage> createState() =>
      _ActivityRecognitionPageState();
}

class _ActivityRecognitionPageState extends State<ActivityRecognitionPage> {
  int activityIndex = 0;

  final List<String> standingFrames = const ['assets/images/standing.png'];
  final List<String> walkingFrames = const [
    'assets/images/walking_frames/walking_0.png',
    'assets/images/walking_frames/walking_1.png',
    'assets/images/walking_frames/walking_2.png',
    'assets/images/walking_frames/walking_3.png',
    'assets/images/walking_frames/walking_4.png',
    'assets/images/walking_frames/walking_5.png',
    'assets/images/walking_frames/walking_6.png'
  ];
  final List<String> runningFrames = const [
    'assets/images/running_frames/running_0.png',
    'assets/images/running_frames/running_1.png',
    'assets/images/running_frames/running_2.png',
    'assets/images/running_frames/running_3.png',
    'assets/images/running_frames/running_4.png',
    'assets/images/running_frames/running_5.png',
    'assets/images/running_frames/running_6.png'
  ];
  Map<String, List<String>> activites = {};

  ///////////////
  static final clientID = 0;
  BluetoothConnection? connection;

  String _messageBuffer = '';

  bool isConnecting = true;
  bool get isConnected => (connection?.isConnected ?? false);

  bool isDisconnecting = false;

  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 15), () {
      if (isConnecting) {
        //Navigator.of(context).pop(this);
      }
    });
    BluetoothConnection.toAddress(widget.server.address).then((_connection) {
      print('Connected to the device');
      connection = _connection;
      setState(() {
        isConnecting = false;
        isDisconnecting = false;
      });

      connection!.input!.listen((Uint8List data) {
        print('Data incoming: ${ascii.decode(data)}');
        activityIndex = int.parse(ascii.decode(data));
        setState(() {
          activityIndex = activityIndex - 1;
        });
      }).onDone(() {
        // Example: Detect which side closed the connection
        // There should be `isDisconnecting` flag to show are we are (locally)
        // in middle of disconnecting process, should be set before calling
        // `dispose`, `finish` or `close`, which all causes to disconnect.
        // If we except the disconnection, `onDone` should be fired as result.
        // If we didn't except this (no flag set), it means closing by remote.
        if (isDisconnecting) {
          print('Disconnecting locally!');
        } else {
          print('Disconnected remotely!');
        }
        if (mounted) {
          setState(() {});
        }
      });
    }).catchError((error) {
      print('Cannot connect, exception occured');
      print(error);
    });
  }

  @override
  Widget build(BuildContext context) {
    activites = {
      'Standing': standingFrames,
      'Walking': walkingFrames,
      'Running': runningFrames
    };
    return Scaffold(
      appBar: AppBar(
        title: const Text('Activity Recognition'),
        centerTitle: true,
      ),
      body: Center(
        child: isConnecting
            ? AnimatedTextKit(
                animatedTexts: [
                  TyperAnimatedText('...Connecting',
                      textStyle: const TextStyle(
                          fontSize: 25, color: Colors.redAccent)),
                ],
                onTap: () {
                  print("Tap Event");
                },
              )
            : ActivityWidget(
                activityTitle: activites.keys.elementAt(activityIndex),
                activityImages: activites.values.elementAt(activityIndex)),
      ),
    );
  }
}
