import 'dart:async';

import 'package:activity_recognition_app/screens/activity_recognition_page.dart';
import 'package:activity_recognition_app/utility/utilities.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

import './chat_page.dart';
import './discovery_page.dart';
import './select_bonded_device_page.dart';

// import './helpers/line_chart.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPage createState() => _MainPage();
}

class _MainPage extends State<MainPage> {
  BluetoothState _bluetoothState = BluetoothState.UNKNOWN;

  String _name = "...";

  Timer? _discoverableTimeoutTimer;

  bool _autoAcceptPairingRequests = false;

  @override
  void initState() {
    super.initState();

    // Get current state
    FlutterBluetoothSerial.instance.state.then((state) {
      setState(() {
        _bluetoothState = state;
      });
    });

    Future.doWhile(() async {
      // Wait if adapter not enabled
      if ((await FlutterBluetoothSerial.instance.isEnabled) ?? false) {
        return false;
      }
      await Future.delayed(const Duration(milliseconds: 0xDD));
      return true;
    }).then((_) {
      // Update the address field
      FlutterBluetoothSerial.instance.address.then((address) {
        setState(() {});
      });
    });

    FlutterBluetoothSerial.instance.name.then((name) {
      setState(() {
        _name = name!;
      });
    });

    // Listen for futher state changes
    FlutterBluetoothSerial.instance
        .onStateChanged()
        .listen((BluetoothState state) {
      setState(() {
        _bluetoothState = state;

        // Discoverable mode is disabled when Bluetooth gets disabled
        _discoverableTimeoutTimer = null;
      });
    });
  }

  @override
  void dispose() {
    FlutterBluetoothSerial.instance.setPairingRequestHandler(null);
    _discoverableTimeoutTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Activity recognition'),
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          const Divider(),
          SwitchListTile(
            title: const Text('Enable Bluetooth'),
            value: _bluetoothState.isEnabled,
            onChanged: (bool value) {
              // Do the request and update with the true value then
              future() async {
                // async lambda seems to not working
                if (value) {
                  await FlutterBluetoothSerial.instance.requestEnable();
                } else {
                  await FlutterBluetoothSerial.instance.requestDisable();
                }
              }

              future().then((_) {
                setState(() {});
              });
            },
          ),
          ListTile(
            title: const Text('Bluetooth status'),
            subtitle: Text(_bluetoothState.toString()),
            trailing: ElevatedButton(
              child: const Text('Settings'),
              onPressed: () {
                FlutterBluetoothSerial.instance.openSettings();
              },
            ),
          ),
          ListTile(
            title: const Text('Local adapter name'),
            subtitle: Text(_name),
            onLongPress: null,
          ),
          const Divider(),
          ListTile(
            title: ElevatedButton(
                child: const Text('Explore discovered devices'),
                onPressed: () async {
                  checkPermission(context).then((value1) => {
                        if (value1)
                          bluetoothLocationAreEnable(context)
                              .then((value2) async {
                            if (value2) {
                              final BluetoothDevice selectedDevice =
                                  await Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return const DiscoveryPage();
                                  },
                                ),
                              );
                              if (selectedDevice != null) {
                                print('Discovery -> selected ' +
                                    selectedDevice.address);
                              } else {
                                print('Discovery -> no device selected');
                              }
                            }
                          })
                      });
                }),
          ),
          ListTile(
            title: ElevatedButton(
              child: const Text('Connect to paired device'),
              onPressed: () async {
                checkPermission(context).then((value1) => {
                      if (value1)
                        bluetoothLocationAreEnable(context)
                            .then((value2) async {
                          if (value2) {
                            final BluetoothDevice selectedDevice =
                                await Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) {
                                  return const SelectBondedDevicePage(
                                      checkAvailability: false);
                                },
                              ),
                            );
                            if (selectedDevice != null) {
                              print('Connect -> selected ' +
                                  selectedDevice.address);
                              _startChat(context, selectedDevice);
                            } else {
                              print('Connect -> no device selected');
                            }
                          }
                        })
                    });
              },
            ),
          ),
        ],
      ),
    );
  }

  void _startChat(BuildContext context, BluetoothDevice server) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return /*ChatPage(server: server);*/ ActivityRecognitionPage(
            server: server,
          );
        },
      ),
    );
  }
}
