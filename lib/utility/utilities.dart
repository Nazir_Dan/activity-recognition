import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

Future<bool> checkPermission(BuildContext ctx) async {
  //This for check Location permission
  //if IOS app did not work check this code (permission.locationWhenInUse.status)
  if (await Permission.location.request().isGranted) {
    return Future<bool>.value(true);
  } else {
    var status = await Permission.location.status;
    if (status.isDenied) print("Permission Denied");
    if (status.isDenied || status.isPermanentlyDenied || status.isRestricted) {
      showDialog(
          context: ctx,
          builder: (BuildContext context) => CupertinoAlertDialog(
                title: const Text('Location Permission'),
                content: const Text(
                    'This app needs Location access for bluetooth connection  '),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: const Text(
                      'Deny',
                      style: TextStyle(color: Colors.redAccent),
                    ),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                  CupertinoDialogAction(
                      child: const Text('Settings'),
                      onPressed: () {
                        Navigator.of(context).pop();
                        openAppSettings();
                      }),
                ],
              ));
    }
    if (status.isGranted) {
      return Future<bool>.value(true);
    }
  }

  return Future<bool>.value(false);
  //print(status);
}

Future<bool> bluetoothLocationAreEnable(BuildContext ctx) async {
  bool locationIsEnable = await Permission.location.serviceStatus.isEnabled;
  bool? bluetoothIsEnable = await FlutterBluetoothSerial.instance.isEnabled;
  if (locationIsEnable && bluetoothIsEnable!) {
    print("locationIsEnable");
    print("bluetoothIsEnable");
    //bluetoothConnection(ctx);
    return true;
  } else {
    if (!locationIsEnable && bluetoothIsEnable!) {
      showToast("Enable Location to connect");
      print("not locationIsEnable");
    }
    if (!bluetoothIsEnable! && locationIsEnable) {
      showToast("Enable Bluetooth to connect");
      print(" not bluetoothIsEnable");
    }
    if (!bluetoothIsEnable && !locationIsEnable) {
      showToast("Enable Bluetooth and Location to connect");
      print(" not bluetooth&LocationIsEnable");
    }
    return false;
  }
}

void showToast(String text) {
  Fluttertoast.showToast(
      msg: text,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black38,
      textColor: Colors.white,
      fontSize: 16.0);
}
