import 'package:flutter/material.dart';
import 'package:image_sequence_animator/image_sequence_animator.dart';

class ActivityWidget extends StatefulWidget {
  final String activityTitle;
  final List<String> activityImages;

  // ignore: use_key_in_widget_constructors
  const ActivityWidget(
      {required this.activityTitle, required this.activityImages});

  @override
  State<ActivityWidget> createState() => _ActivityWidgetState();
}

class _ActivityWidgetState extends State<ActivityWidget> {
  ImageSequenceAnimatorState? offlineImageSequenceAnimator;
  void onOfflineReadyToPlay(ImageSequenceAnimatorState _imageSequenceAnimator) {
    offlineImageSequenceAnimator = _imageSequenceAnimator;
  }

  void onOfflinePlaying(ImageSequenceAnimatorState _imageSequenceAnimator) {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      width: 200,
      decoration: BoxDecoration(
        color: Colors.blueGrey[200],
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          widget.activityTitle == 'Standing'
              ? Padding(
                  padding: const EdgeInsets.all(8),
                  child: Image(image: AssetImage(widget.activityImages[0])),
                )
              : widget.activityTitle == 'Walking'
                  ? Center(
                      child: ImageSequenceAnimator(
                        "assets/images/walking_frames",
                        "walking_",
                        0,
                        1,
                        "png",
                        7,
                        fps: 12,
                        key: const Key("offline"),
                        //fullPaths: widget.activityImages,
                        onReadyToPlay: onOfflineReadyToPlay,
                        onPlaying: onOfflinePlaying,
                        isLooping: true,
                      ),
                    )
                  : Center(
                      child: ImageSequenceAnimator(
                        "assets/images/running_frames",
                        "running_",
                        0,
                        1,
                        "png",
                        7,
                        fps: 25,
                        key: const Key("offline"),
                        //fullPaths: widget.activityImages,
                        onReadyToPlay: onOfflineReadyToPlay,
                        onPlaying: onOfflinePlaying,
                        isLooping: true,
                      ),
                    ),
          // Padding(
          //   padding: const EdgeInsets.all(8.0),
          //   child:
          //   Image(
          //     image: activityImage.image,
          //     //fit: BoxFit.cover,
          //   ),
          // ),
          const SizedBox(
            height: 5,
          ),
          Container(
            height: 30,
            width: double.infinity,
            decoration: BoxDecoration(color: Colors.blueGrey[400]),
            child: Center(
                child: Text(
              widget.activityTitle,
              style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
            )),
          ),
        ],
      ),
    );
  }
}
